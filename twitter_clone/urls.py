from django.conf.urls import include, url
from django.urls import path

from django.contrib import admin
admin.autodiscover()

import clone.views

urlpatterns = [
    url(r'^$', clone.views.index, name='index'),
    path('signup/', clone.views.SignupView.as_view()),
    path('posts/', clone.views.PostView.as_view()),
    path('users/<id>/', clone.views.user_show),
    path('users/', clone.views.users),
    path('accounts/', include('django.contrib.auth.urls')),
    path('admin/', admin.site.urls),
]
