from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import Http404,HttpResponseRedirect
from django.contrib.auth.models import User
from clone.models import Post
from django.views import View
from django.utils.decorators import method_decorator
from datetime import datetime
from django.contrib.auth import authenticate, login

@login_required
def index(request):
    posts = Post.objects.all()
    return render(request, 'index.html', { 'posts': posts})

class PostView(View):
    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        text = request.POST['post']
        post = Post(text=text, user=request.user, created_at=datetime.now())
        post.save()
        return HttpResponseRedirect('/')

@login_required
def user_show(request, id):
    try:
        user = User.objects.get(id=id)
        user_posts = Post.objects.filter(user=user)
    except User.DoesNotExist:
        raise Http404
    return render(
        request,
        'user_show.html',
        {
            'user' : user,
            'user_posts': user_posts
        })

@login_required
def users(request):
    all_users = User.objects.all()
    return render(
        request,
        'user_list.html',
        {'users' : all_users}
    )

class SignupView(View):
    def get(self, request, *args, **kwargs):
        return render(
            request,
            'signup.html',
            {}
        )
    def post(self, request, *args, **kwargs):
        email = request.POST['email']
        password = request.POST['password']
        user = User.objects.create_user(
                              username=email,
                              email=email,
                              password=password)
        user = authenticate(
                 username = email,
                 password = password)
        if user is not None:
            login(request, user)

        return HttpResponseRedirect('/')
