from django.contrib.auth.models import User
from django.db import models

class Post(models.Model):
    text = models.CharField(max_length=140)
    created_at = models.DateTimeField('date published')
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )

class Follow(models.Model):
    follower = models.ForeignKey(User, on_delete=models.CASCADE,
            related_name='follower')
    followee = models.ForeignKey(User, on_delete=models.CASCADE,
            related_name='followee')
