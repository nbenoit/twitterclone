from django.test import TestCase
from django.contrib.auth.models import User
from clone.models import Post

class Tests(TestCase):
    def test_signup(self):
        previous_users = User.objects.all().count()
        response = self.client.post('/signup/',
                       { "email":"test@example.com", "password":"asdf"})
        self.assertRedirects(response, '/')
        self.assertEqual(User.objects.all().count(), previous_users + 1)

    def test_create_post(self):
        User.objects.create_user(username="user", email="test@example.com", password="test")
        self.client.login(username="user", password="test")
        previous_posts = Post.objects.all().count()
        response = self.client.post("/posts/",
                       { "post":"A very clever post"})
        self.assertRedirects(response, '/')
        self.assertEqual(Post.objects.all().count(), previous_posts + 1)
